## TwoTrackMVA
The training procedure for the TwoTrackMVA is found in https://github.com/niklasnolte/HLT_2Track.

The event types used for training can be seen in [here](https://github.com/niklasnolte/HLT_2Track/blob/main/hlt2trk/utils/config.py#L384).

The model exported from there goes into Allen/input/parameters/two_track_mva_model.json
